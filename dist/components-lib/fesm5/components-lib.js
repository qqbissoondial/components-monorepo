import { Component, NgModule } from '@angular/core';

/**
 * @fileoverview added by tsickle
 * Generated from: lib/components-lib.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var ComponentsLibComponent = /** @class */ (function () {
    function ComponentsLibComponent() {
    }
    /**
     * @return {?}
     */
    ComponentsLibComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    ComponentsLibComponent.decorators = [
        { type: Component, args: [{
                    selector: 'comp-components-lib',
                    template: "\n    <p>\n      components-lib works!\n    </p>\n  "
                }] }
    ];
    /** @nocollapse */
    ComponentsLibComponent.ctorParameters = function () { return []; };
    return ComponentsLibComponent;
}());

/**
 * @fileoverview added by tsickle
 * Generated from: lib/components-lib.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var ComponentsLibModule = /** @class */ (function () {
    function ComponentsLibModule() {
    }
    ComponentsLibModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [ComponentsLibComponent],
                    imports: [],
                    exports: [ComponentsLibComponent]
                },] }
    ];
    return ComponentsLibModule;
}());

/**
 * @fileoverview added by tsickle
 * Generated from: public-api.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: components-lib.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { ComponentsLibModule, ComponentsLibComponent as ɵa };
//# sourceMappingURL=components-lib.js.map
